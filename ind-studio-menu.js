// This script creates a new view for ind-studio-menu
// Author:
// Creation Date:
// Latest Update:
// Version: 0.0.1
// Dependencies:
//  - command-line-args 
//  - command-line-usage
//  - child_process
//  - chalk

const commandLineArgs = require("command-line-args");
const commandLineUsage = require("command-line-usage");
const chalk = require("chalk");

// Parameters definition
const optionDefinitions = [
    { name: "name", alias: "n", type: String, description: "Name of the component. Required.", typeLabel: "{underline component-name}" },
    { name: "title", alias: "t", type: String, description: "Title for Menu Option. In case no translation is needed.", typeLabel: "{underline string}" },
    { name: "icon", alias: "i", type: String, description: "Icon for Menu Option. (Default: dashboard)", typeLabel: "{underline material-icon}", defaultValue: "dashboard" },
    { name: "language", alias: "l", type: String, multiple: true, description: "Languages for Menu Option. Example -l us=Test -l es=Prueba", typeLabel: "{underline us= | es= | cn= | etc}" },
    { name: "order", alias: "o", type: Number, description: "Menu order (position) from 0 to N. (Default: 0)", typeLabel: "{underline integer}", defaultValue: 0 },
    { name: "parent", alias: "p", type: String, description: "Menu Parent Title.", typeLabel: "{underline parent-title}" },
    { name: "dry-run", alias: "d", type: Boolean, description: "Dry run." },
    { name: "verbose", alias: "v", type: Boolean, description: "More output." },
    { name: "help", alias: "h", type: Boolean, description: "Print this usage guide." },
];

///////////////////////////////////////////////////////////////////////////////
let allOK = false;
const basePath = "./src/app";
const viewsPath = "views";
const fileNameMenuDev = "menu-options.config.dev.json"
// Parse options
const options = commandLineArgs(optionDefinitions, { camelCase: true });

if (options.dryRun == true) {
    console.log(`${chalk.yellow("Dry run execution. Files will not be written.")}`);
}
if (options.name == undefined && options.help == undefined) {
    console.error(`[ind-studio-menu] ${chalk.red("ERROR:")} Parameter --name (or -n) is Required. Component Name missing. Use -h for help. Aborting`);
    process.exit(1);
}
options.titleSpecified = true;
if (options.title == undefined) {
    options.titleSpecified = false;
    options.title = options.name;
}

///////////////////////////////////////////////////////////////////////////////
// Show Help
if (options.help) {
    const sections = [
        { header: "Industrial Studio Menu CLI", content: "CLI to manage menu options." },
        { header: "Options", optionList: optionDefinitions },
        {
            content: [
                "Example:",
                "",
                `   ${chalk.blue("node ind-studio-menu -n myNewPage -l us=\"New Page\" -l es=\"Pagina Nueva\" -i 3d_rotation -o 2")}`,
                "   Will create a component \"MyNewPageComponent\" in ./src/app/views/my-new-page, add routing, add menu entry (as third menu option) with icon \"3d_rotation\" and add languages entries to english and spanish",
                "",
                `   ${chalk.blue("node ind-studio-menu --name myNewPage2 --language us=\"New Page 2\" --language es=\"Pagina Nueva 2\" --icon offline_bolt --parent menu.items.Tools")}`,
                "   Will create a component \"MyNewPage2Component\" in ./src/app/views/my-new-page2, add routing, add menu entry (child of Tools) with icon \"offline_bolt\" and add languages entries to english and spanish",
                "",
                "",
                "{italic (c) 2018 ANT Automation LLC.}"], raw: true
        }
    ];
    const usage = commandLineUsage(sections);
    console.log(usage);
    process.exit(0);
}

try {
    ({ exec, execSync } = require("child_process"));
    fs = require("fs");
    allOK = true;
} catch (error) {
    allOK = false;
    if (error.code === "MODULE_NOT_FOUND") {
        console.error(
            '[ind-studio-menu] Node module "child_process" was not found. Make sure Nodejs is installed.'
        );
    } else {
        console.error("[ind-studio-menu] Error: %o", error);
    }
}

if (allOK) {
    initializeProcess();
}

///////////////////////////////////////////////////////////////////////////////
function initializeProcess() {
    var moduleRoute = camelCaseToDash(options.name);
    var componentPath = basePath + "/" + viewsPath + "/" + moduleRoute;
    if (fs.existsSync(componentPath)) {
        console.error(`view: ${chalk.blue(options.name)} exists on ${chalk.blue(basePath + "/" + viewsPath)}. Aborting.`);
        return;
    }

    if (!createComponent(viewsPath, options.name)) {
        console.log("Exiting");
        process.exit(0);
    }

    if (!addRouting('app-routing.module.ts', viewsPath, options.name)) {
        console.log("Exiting");
        process.exit(0);
    }

    if (!addEntryComponents('app-routing.module.ts', options.name)) {
        console.log("Exiting");
        process.exit(0);
    }

    if (!addMenuEntry(fileNameMenuDev, options.title, options.titleSpecified, options.icon, camelCaseToDash(options.name))) {
        console.log("Exiting");
        process.exit(0);
    }
    if (!addi18nEntry(options.language, options.title, options.titleSpecified)) {
        console.log("Exiting");
        process.exit(0);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Create Component (angular)
function createComponent(path, name) {
    var command = "ng g c " + path + "/" + name;
    process.stdout.write(`Executing command ${chalk.blue(command)} : `);
    try {
        if (!options.dryRun) {
            execSync(command);
        }
        logPassed();
    } catch (ex) {
        logFailed();
        console.error(`[ind-studio-menu] Error: Node couldn\'t execute the command: ${chalk.blue(command)}`);
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// Modify app-routing.module.ts.
// Add import and route
function addRouting(sourceFileName, path, name) {

    var fileName = basePath + '/' + sourceFileName;
    process.stdout.write(`Adding Route to the content of ${chalk.blue(fileName)} :`);
    try {
        var fileContent = fs.readFileSync(fileName, 'utf8');

        var moduleComponent = upperCaseFirst(name).concat("Component");
        var moduleRoute = camelCaseToDash(name);

        // Add import (at beginning)
        let addLine = `import { ${moduleComponent} } from './${path}/${moduleRoute}/${moduleRoute}.component'; // Added by script ind-studio-menu\r\n`;
        fileContent = addLine.concat(fileContent);

        // Add route to const
        var routesRegex = /(?:\broutes(?:\s|\t)*=(?:\s|\t)*(?:\n|\r)*\[(?:\s|\t)*(?:\n|\r)*)((?:\s|\t)*[^\]]*(?:\n|\r)*)*(?:\])/gi;
        var routesFound = routesRegex.exec(fileContent);
        var newLine = '';

        if (routesFound.length > 1) {
            var newRoutes = '';
            if (routesFound[1] == undefined) {
                newLine = '{ path: \'' + moduleRoute + '\', component: ' + moduleComponent + ' }';
                newRoutes = newLine;
            }
            else {

                var routesArray = routesFound[1]
                    .replace(/({|\s|\t|\n|\r)*/g, '')
                    .split(/(?:}),*/)
                    .reduce(function (routes, r) {
                        if (r) { routes.push('{ ' + r.replace(/(:|,)/g, '$1 ') + ' }'); }
                        return routes;
                    }, []);
                var newLine = '{ path: \'' + moduleRoute + '\', component: ' + moduleComponent + ' }';
                routesArray.push(newLine);
                newRoutes = routesArray.join(',\n    ');
            }
            fileContent = fileContent.replace(routesRegex, 'Routes = [\n    ' + newRoutes + '\n]');
        }

        logPassed();
        if (options.verbose) {
            console.log(`${chalk.yellow("Add import       : ")} ${chalk.blue(addLine.replace("\r", "").replace("\n", ""))}`)
            console.log(`${chalk.yellow("Pushing to Routes: ")} ${chalk.blue(newLine)}`)
            console.log(`${chalk.yellow("--------------------------- RESULT FILE: ")}${fileName} ${chalk.yellow("---------------------------")}`)
            console.log(`${fileContent}`)
            console.log(`-------------------------------------------------------------------`)
        }

        // Write routing js file
        if (!options.dryRun) {
            process.stdout.write(`Writing File ${chalk.blue(fileName)} :`);
            fs.writeFileSync(fileName, fileContent);
            logPassed();
        }
    } catch (error) {
        logFailed();
        allOK = false;
        console.error(`[ind-studio-menu:${chalk.yellow(getFunctionName())}] ${chalk.red("Error:")} ${error.message}`);
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////
// Modify app.module.ts
// Add entryComponents
function addEntryComponents(sourceFileName, name) {
    var fileName = basePath + '/' + sourceFileName;
    process.stdout.write(`Adding entryComponent to content of ${chalk.blue(fileName)} :`);
    try {
        var fileContent = fs.readFileSync(fileName, 'utf8');
        if (fileContent == undefined || fileContent.length < 100) {
            logFailed();
            allOK = false;
            return;
        }
        var moduleComponent = upperCaseFirst(name).concat("Component");

        // Add route to const
        var entryComponentsRegex = /(?:\bentryComponents(?:\s|\t)*:(?:\s|\t)*(?:\n|\r)*\[(?:\s|\t)*(?:\n|\r)*)((?:\s|\t)*[^\]]*(?:\n|\r)*)*(?:\])/gi;
        var entriesFound = entryComponentsRegex.exec(fileContent);
        if (entriesFound.length > 1) {
            var newEntries = '';
            if (entriesFound[1] == undefined) {
                newEntries = [moduleComponent];
            }
            else {
                var entriesArray = entriesFound[1]
                    .replace(/(\s|\t|\n|\r)*/g, '')
                    .split(/,/);

                entriesArray.push('' + moduleComponent + '');
                newEntries = '\t' + entriesArray.join(',\n' + '\t\t');
            }
            fileContent = fileContent.replace(entryComponentsRegex, 'entryComponents: [\n    ' + newEntries + '\n\t]');

        }
        logPassed();
        if (options.verbose) {
            console.log(`${chalk.yellow("Pushing to entryComponents: ")} ${chalk.blue(moduleComponent)}`)
            console.log(`${chalk.yellow("--------------------------- RESULT FILE: ")}${fileName} ${chalk.yellow("---------------------------")}`)
            console.log(`${fileContent}`)
            console.log(`-------------------------------------------------------------------`)
        }

        // Write the file
        if (!options.dryRun) {
            process.stdout.write(`Writing File ${chalk.blue(fileName)} :`);
            fs.writeFileSync(fileName, fileContent);
            logPassed()
        }
    } catch (error) {
        logFailed();
        allOK = false;
        console.error(`[ind-studio-menu:${chalk.yellow(getFunctionName())}] ${chalk.red("Error:")} ${error.message}`);
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////
function addMenuEntry(fileName, title, titleSpecified, icon, route) {
    try {
        var configFullName = "./src/assets/settings/" + fileName;
        process.stdout.write(`Adding menu entry to content of ${chalk.blue(configFullName)} :`);
        var moduleRoute = route;
        var optionMenu =
        {
            "title": (titleSpecified) ? title : "menu.items." + title,
            "routerLink": "/" + moduleRoute,
            "icon": icon
        }
        var fileContent = fs.readFileSync(configFullName, 'utf8');

        var fileContentObject = JSON.parse(fileContent);
        if (options.parent == undefined) {
            fileContentObject.menuOptions.splice(options.order, 0, optionMenu);
            fileContent = JSON.stringify(fileContentObject, null, 2);
        }
        else {
            // Search For Parent. Find first with "title": endswith)
            // if parent is not a submenu ADD to root
            // if parent is not found ADD to root
            var found = fileContentObject.menuOptions.find(function (element) {
                return element.title.endsWith(options.parent)
            });
            if (found) {
                optionMenu.parentId = found.id;
                fileContentObject.menuOptions.splice(options.order, 0, optionMenu);
            }
            else {
                logWarning();
                console.log(`${chalk.yellow(getFunctionName())}: Error, parent not found. Creating entry in root menu - top position`);
                fileContentObject.menuOptions.splice(options.order, 0, optionMenu);
                allOK = false;
            }
            fileContent = JSON.stringify(fileContentObject, null, 2);
        }
        if (allOK) logPassed();
        allOK = true;
        if (options.verbose) {
            console.log(`${chalk.yellow("Pushing to menuOptions: ")} ${optionMenu.title}`)
            console.log(`${chalk.yellow("--------------------------- RESULT FILE: ")}${configFullName} ${chalk.yellow("---------------------------")}`)
            console.log(`${fileContent}`)
            console.log(`-------------------------------------------------------------------`)
        }
        // Write the file
        if (!options.dryRun) {
            process.stdout.write(`Writing File ${chalk.blue(configFullName)} :`);
            fs.writeFileSync(configFullName, fileContent);
            logPassed()
        }
    } catch (error) {
        logFailed();
        allOK = false;
        console.error(`[ind-studio-menu:${chalk.yellow(getFunctionName())}] ${chalk.red("Error:")} ${error.message}`);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
function addi18nEntry(languagesArray, key) {
    if (languagesArray == undefined) return;
    if (key == undefined) return;
    try {
        var pathConfig = "./src/assets/i18n/";
        languagesArray.forEach(function (element) {
            var languageAbbrev = element.split("=")[0].trim();
            var languageValue = element.split("=")[1].trim();

            var langFileName = pathConfig + "app." + languageAbbrev + ".json";
            process.stdout.write(`Adding language entry file to ${chalk.blue(langFileName)} :`);

            var configLangFile = fs.readFileSync(langFileName, 'utf8');
            var config = JSON.parse(configLangFile);
            config.menu.items[key] = languageValue;
            var resultFile = JSON.stringify(config, null, 2);
            logPassed();
            if (options.verbose) {
                console.log(`${chalk.yellow("Pushing language entry: ")} ${key}: ${languageValue}`)
                console.log(`${chalk.yellow("--------------------------- RESULT FILE: ")}${langFileName} ${chalk.yellow("---------------------------")}`)
                console.log(`${resultFile}`)
                console.log(`-------------------------------------------------------------------`)
            }
            // Write language file
            if (!options.dryRun) {
                process.stdout.write(`Writing File ${chalk.blue(langFileName)} :`);
                fs.writeFileSync(langFileName, resultFile);
                logPassed()
            }
        })
    } catch (error) {
        logFailed();
        allOK = false;
        console.error(`[ind-studio-menu:${chalk.yellow(getFunctionName())}] ${chalk.red("Error:")} ${error.message}`);
    }
    return true;
}

function upperCaseFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function logPassed() {
    console.log(chalk.green(" [OK]"));
}

function logFailed() {
    console.log(chalk.red(" [ERROR]"));
}

function logWarning() {
    console.log(chalk.redBright(" [WARN]"));
}

function camelCaseToDash(str) {
    return str.replace(/([a-z0-9A-Z])(?=[A-Z])/g, '$1-').toLowerCase()
}

function getFunctionName() {
    return arguments.callee.caller.name;
}

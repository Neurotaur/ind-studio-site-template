import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { SignalRConfiguration, SignalRModule } from 'ng2-signalr';
import { IndStudioCommonModule, IndStudioTranslateService, getSignalRConfiguration } from '@ind-studio/common';
import { IndStudioMenuModule, IndStudioMenuSettings, IndStudioMenuInitRoutingModule } from '@ind-studio/menu';
import { IndStudioCoreScreensModule } from '@ind-studio/core-screens';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { AppRoutingModule } from './app-routing.module';
import { TopBarComponent } from './top-bar/top-bar.component';
import { IndStudioMenuTopBarService } from '@ind-studio/menu';

registerLocaleData(localeEs, 'es');

export function TranslateLoaderFactory(http: HttpClient, indStudioTranslateServ: IndStudioTranslateService) {
    return indStudioTranslateServ.getLoader(http, ['app']);
}

export function getSignalRConfig(): SignalRConfiguration {
    return getSignalRConfiguration();
}

@NgModule({
    imports: [
        BrowserModule,
        IndStudioCommonModule,
        IndStudioMenuModule,
        IndStudioCoreScreensModule,
        AppRoutingModule,
        IndStudioMenuInitRoutingModule,
        SignalRModule.forRoot(getSignalRConfig),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: TranslateLoaderFactory,
                deps: [HttpClient, IndStudioTranslateService]
            }
        })
    ],
    declarations: [
        AppComponent,
        TopBarComponent
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ],
    entryComponents: [
        TopBarComponent
    ]
})
export class AppModule {

    /* set up the translation service */
    constructor(private indStudioSettings: IndStudioMenuSettings, private translateServ: TranslateService,
        private indStudioTranslateServ: IndStudioTranslateService, private topbarService: IndStudioMenuTopBarService) {
        indStudioTranslateServ.addLangs(translateServ, indStudioSettings.languages);
        topbarService.setTopBarComponent(TopBarComponent);
    }
}

import { Component, OnInit } from '@angular/core';
import { IndStudioMenuTitleService } from '@ind-studio/menu';
import { IndStudioScadaService, getConfigFromFile } from '@ind-studio/common';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  providers: [ DecimalPipe ]
})
export class TopBarComponent implements OnInit {
    tags: any;
    title: string;
    tagList = [];
    config: any;

    constructor(
        private menuTitleService: IndStudioMenuTitleService,
        private scadaService: IndStudioScadaService,
        private decimalPipe: DecimalPipe
        ) {
            this.config = getConfigFromFile('assets/settings/app.config.dev.json');
            this.tags = this.config.tags || [];
        }

    ngOnInit() {
        setTimeout(() => {
            this.menuTitleService.title.subscribe((val: string) => {
                this.title = (val || '');
            });
        });

        this.scadaService.tagsValueListObserver().subscribe((values: Array<any>) => {
            this.tagList = [];
            this.tags.forEach(element => {
                const tagElement = values.filter(x => x.Name === element.tagName);
                const value = tagElement && tagElement[0] ? tagElement[0].Value : 0;
                const tag = {
                    title: element.title ? element.title : element.tagName,
                    value: !isNaN(Number(value)) && element.format ?
                        this.decimalPipe.transform(value, element.format, this.config.locale || 'en') :
                        value,
                    unit: element.unit ? element.unit : ''
                };

                this.tagList.push(tag);
            });
        });
    }

}

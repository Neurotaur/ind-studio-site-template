# ind-studio-site-template

This is a starter proyect that contains the core screens for the Ant Industrial Studio Frontend Framework

## Requirements

This package requires the libraries contained in the `@ind-studio/common`, `@ind-studio/menu`, `@ind-studio/core-screens`, `@ind-studio/trend` and `@ind-studio/widgets` packages.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

### Check that you have node and npm installed

To check if you have Node.js installed, run this command in your terminal:

```bash
node -v
```

To confirm that you have npm installed you can run this command in your terminal:

```
npm -v
```

if you are missing nodejs (or npm) install the nodejs package from here [https://nodejs.org/en/](https://nodejs.org/en/)

### Check that you have angular-cli installed

To check if you have angular-cli (ng) installed, run this command in your terminal:

```bash
ng -v
```

if you are missing `ng` cli, run the following command in your terminal:

```bash
npm install -g @angular/cli
```

More information in: [https://cli.angular.io/](https://cli.angular.io/)

## Build

Run `npm login`, user: `ant-auto`, password: `6mA-2kV-x10`

```bash
$ npm login
Username: ant-auto
Password: 6mA-2kV-x10
Email: (this IS public) npm@ant-automation.com
Logged in as ant-auto on https://registry.npmjs.org/.
```

Run `npm install`

Run `ng build --prod` to build the project. The build artifacts will be stored in the `dist/` directory. Remember to use the `--prod` flag for a production build.

## Development server (Sandbox app)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## How to add a new view

### Adding a new view using ind-studio-menu script

1. Execute command `node ind-studio-menu -h` for help

1. Example add a page called `MyTestPage` with two language options:

    ```bash
    node ind-studio-menu -n myTestPage -l us="Test Page" -l es="Pagina Prueba" -i 3d_rotation -o 2
    ```

    Will add a new page in the root menu, position 2, with icon `3d_rotation` and with the following titles for the specified language: `Test Page` and `Pagina Prueba`.

    For more icons name, visit: [material icons page](https://material.io/tools/icons/)

    Component name: `MyTestPageComponent`, in `./src/app/views/my-test-page`

    Routing is automatically added: `/my-test-page`

    Another example:

    ```bash
    node ind-studio-menu --name myNewPage2 --language us="New Page 2" --language es="Pagina Nueva 2" --icon offline_bolt --parent menu.items.Tools
    ```

    Will create a component named "MyNewPage2Component" in ./src/app/views/my-new-page2, add routing, add menu entry (child of Tools) with icon \"offline_bolt\" and add languages entries to english and spanish

    > TIP: Use `--dry-run` and `--verbose` parameters to see all changes the script will produce without modifying the files.

### Adding a new view manually

1. Create the view component. From the console, navigate to the project folder (ind-studio-template):

        ng g c test

    There will be a new folder with the new component’s files.

1. Add the new component to the routing. Modify `app-routing.module.ts` file:

        import {  TestComponent   } from './test/test.component';
        ...
        const routes: Routes = [
        ...
        { path: 'test', component: TestComponent },
        ...
        ]

    Then modify `app.module.ts` file. 
    Add the `IndStudioContainerModule` to the main module:

        import { IndStudioContainerModule } from '@ind-studio/container';
        ...
        @NgModule({
            imports: [
                BrowserModule,
                MatInputModule,
                IndStudioCommonModule,
                IndStudioMenuModule,
                IndStudioCoreScreensModule,
                IndStudioContainerModule,
                ...
            ]
            ...
        })

    Add the new component to the entryComponents:

        @NgModule({
        ...
        entryComponents: [
                ...
            TestComponent
            ]
        ...
        })

1. Add the new component to the menu.
    Modify the following files: `assets/settings/menu-options.config.dev.json` and `menu-options.config.prod.json`. In the hierarchy, add these lines to menuOptions:

        {
            "title": "menu.items.TestView",
            "routerLink": "/test",
            "icon": "dashboard"
        },


    The routerLink attribute must be the same as in 'path', in step 2. The icon can be chosen from https://material.io/tools/icons/

    If a submenu is needed, add the following lines:

        {
            "id": 40,
            "title": "menu.items.Submenu",
            "icon": "build",
            "hasSubMenu": true
        },
        {
            "title": "menu.items.TestView",
            "routerLink": "/submenu/test",
            "icon": "dashboard",
            "parentId": 40
        },

1. Modify the language files. Modify all files in `src/assets/i18n/`. Add all the defined items, in this example, "menu.items.TestView" and "menu.items.Submenu". 

        {
            "menu": {
                "items": {
                    "Submenu": "Sub menu",
                    "TestView": "Test view",  
                    ...
            }
        }

1. Modify the `test/test.component.html` file.
    First, build the view structure. In this example two rows with three columns each.

        <ind-studio-container>
            <div fxFill fxLayout="column" fxLayoutAlign="flex-start stretch" style="padding: 20px;">
                <div fxFlex="40" fxLayout="row">
                    <div fxFlex="33">
                    
                    </div>
                    <div fxFlex="33">
                    
                    </div>
                    <div fxFlex="33">
                    
                    </div>
                </div>
                <div fxFlex="40" fxLayout="row">
                    <div fxFlex="33">
                    
                    </div>
                    <div fxFlex="33">
                    
                    </div>
                    <div fxFlex="33">

                    </div>
                </div>
            </div>
        </ind-studio-container>

    Then add the widgets in each cell. In this example, there are two types of widgets: round-progress widget and value-widget.

        <ind-studio-container>
            <div fxFill fxLayout="column" fxLayoutAlign="flex-start stretch" style="padding: 20px;">
                <div fxFlex="40" fxLayout="row">
                    <div fxFlex="33">
                        <ind-studio-round-progress-widget
                            [tagName] = "'Tag.Memory.Int'"
                            [max] = "500"
                            [stroke] = "2"
                            [centerPadding] = "0.5"
                            [valueFontSize] = "10"
                            [titleFontSize] = "4"
                            [subtitleFontSize] = "4"
                            [showUnit] = "false"
                            [subtitle] = "'KV'"
                            [title] = "'Example 1'"
                            [formatValue] = "'1.0-0'"
                            [background] = "''"
                            [backgroundList] = "['#F2F2F2']"
                            [conditionList] = "['true']"
                            [bkgChangesWithCenter] = "false"
                        >
                        </ind-studio-round-progress-widget>
                    </div>
                    <div fxFlex="33">
                        <ind-studio-round-progress-widget
                            [tagName] = "'Tag.Memory.Double'"
                            [max] = "500"
                            [stroke] = "2"
                            [centerPadding] = "0.5"
                            [valueFontSize] = "10"
                            [titleFontSize] = "4"
                            [subtitleFontSize] = "4"
                            [showUnit] = "false"
                            [subtitle] = "'KV'"
                            [title] = "'Example 1'"
                            [formatValue] = "'1.0-0'"
                            [background] = "''"
                            [backgroundList] = "['#F2F2F2']"
                            [conditionList] = "['true']"
                            [bkgChangesWithCenter] = "false"
                        >
                        </ind-studio-round-progress-widget>
                    </div>
                    <div fxFlex="33">
                        <ind-studio-round-progress-widget
                            [tagName] = "'Tag.Memory.Int'"
                            [max] = "500"
                            [stroke] = "2"
                            [centerPadding] = "0.5"
                            [valueFontSize] = "10"
                            [titleFontSize] = "4"
                            [subtitleFontSize] = "4"
                            [showUnit] = "false"
                            [subtitle] = "'KV'"
                            [title] = "'Example 1'"
                            [formatValue] = "'1.0-0'"
                            [background] = "''"
                            [backgroundList] = "['#F2F2F2']"
                            [conditionList] = "['true']"
                            [bkgChangesWithCenter] = "false"
                        >
                        </ind-studio-round-progress-widget>
                    </div>
                </div>
                <div fxFlex="40" fxLayout="row">
                    <div fxFlex="33">
                        <ind-studio-meter-widget
                            [tagName] = "'Tag.Memory.Int'"
                            title="Exmple Int"
                            [showUnit]="false"
                            [unit]="'V'"
                            [valueFontSize]="'40px'"
                            [unitFontSize]="'30px'"
                            [titleFontSize]="'20px'"
                            [enableWrite]="'false'"
                            [formatValue] = "'1.0-0'"
                        >
                        </ind-studio-meter-widget>
                    </div>
                    <div fxFlex="33">
                        <ind-studio-meter-widget
                            [tagName] = "'Tag.Memory.Double'"
                            title="Exmple Double"
                            [showUnit]="false"
                            [unit]="'V'"
                            [valueFontSize]="'40px'"
                            [unitFontSize]="'30px'"
                            [titleFontSize]="'20px'"
                            [enableWrite]="'false'"
                            [formatValue] = "'1.2-2'"
                        >
                        </ind-studio-meter-widget>
                    </div>
                    <div fxFlex="33">
                        <ind-studio-meter-widget
                            [tagName] = "'Tag.Memory.Int'"
                            title="Exmple Int"
                            [showUnit]="false"
                            [unit]="'V'"
                            [valueFontSize]="'40px'"
                            [unitFontSize]="'30px'"
                            [titleFontSize]="'20px'"
                            [enableWrite]="'false'"
                            [formatValue] = "'1.0-0'"
                        >
                        </ind-studio-meter-widget>
                    </div>
                </div>
            </div>
        </ind-studio-container>
